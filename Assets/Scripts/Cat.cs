﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Cat
{
	public string nameTexture;
	public float scaleX, scaleY;
	public float positionX, positionY;

	public Cat(string _nameTexture, float _scaleX, float _positionX, float _scaleY, float _positionY)
	{
		this.nameTexture = _nameTexture;
		this.scaleX = _scaleX;
		this.scaleY = _scaleY;
		this.positionX = _positionX;
		this.positionY = _positionY;
	}
}
