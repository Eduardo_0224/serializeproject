﻿using UnityEngine;
using System.Collections;

public class GuiController : MonoBehaviour {

	public GUISkin guiSkin;
	public float posX, posY, width, height;
	public float posSaveX, posSaveY, widthSave, heightSave;
	public float posLoadX, posLoadY, widthLoad, heightLoad;
	public GameObject [] cats;
	public Transform [] spawners;

	void InstantiateCats()
	{
		for (int i = 0; i < cats.Length; i++)
		{
			GameObject cat = Instantiate(cats[i], spawners[i].position, Quaternion.identity) as GameObject;
		}
	}

	void OnGUI()
	{
		GUI.skin = guiSkin;
		if(GUI.Button(new Rect(Screen.width * posX, Screen.height * posY, Screen.width * width, Screen.height * height), "Create Cats"))
		{
			InstantiateCats();
		}

		if(GUI.Button(new Rect(Screen.width * posSaveX, Screen.height * posSaveY, Screen.width * widthSave, Screen.height * heightSave), "Save"))
		{
			// Save
			this.GetComponent<SerializeController>().SaveCatsInGame();

		}

		if(GUI.Button(new Rect(Screen.width * posLoadX, Screen.height * posLoadY, Screen.width * widthLoad, Screen.height * heightLoad), "Load"))
		{
			// Load
			this.GetComponent<SerializeController>().Load();

			
		}
	}
}
