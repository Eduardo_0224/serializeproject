﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SerializeController : MonoBehaviour {

	public GameObject[] catsInScene;
	public List<Cat> arrayOfCats;
	public List<Cat> catsReceive;


	private bool oneTime;
	// Use this for initialization
	void Start () {
	
	}

	public void SaveCatsInGame()
	{
		if(!oneTime)
		{
			oneTime = true;
			catsInScene = GameObject.FindGameObjectsWithTag("Cat");
			for (int i = 0; i < catsInScene.Length; i++)
			{
				Debug.Log(catsInScene[i].name);
				Cat cat = new Cat(catsInScene[i].name, catsInScene[i].gameObject.transform.localScale.x, catsInScene[i].gameObject.transform.localPosition.x,
				                  catsInScene[i].gameObject.transform.localScale.y, catsInScene[i].gameObject.transform.localPosition.y);
				arrayOfCats.Add(cat);
			}

			Save();


		}
	}

	public void Save() {

		BinaryFormatter bf = new BinaryFormatter();
		//Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
		FileStream file = File.Create (Application.persistentDataPath + "/savedCat.gd"); //you can call it anything you want
		bf.Serialize(file, arrayOfCats);
		file.Close();
	}	
	
	public void Load() {
		if(File.Exists(Application.persistentDataPath + "/savedCat.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedCat.gd", FileMode.Open);
			catsReceive = (List<Cat>)bf.Deserialize(file);

			for (int s = 0; s < catsReceive.Count; s++)
			{

								
			}
			file.Close();
		}
	}
}
