﻿using UnityEngine;
using System.Collections;

public class StateCats : MonoBehaviour {

	public Sprite [] sprites;
	private bool changeSprite;
	public int timeRandom;
	public Vector2 [] vectors;

	// Use this for initialization
	void Start ()
	{
		InvokeRepeating("ChangeSprite", 1f, 1f);
	}

	void Update()
	{
		timeRandom = Random.Range(1, 3);
	}

	void ChangeSprite()
	{
		if(timeRandom == 1)
			changeSprite = true;
		else if(timeRandom == 2)
			changeSprite = false;

		this.GetComponent<SpriteRenderer>().sprite = (changeSprite) ? this.GetComponent<SpriteRenderer>().sprite = sprites[0] : this.GetComponent<SpriteRenderer>().sprite = sprites[1];
		Vector2 currentVector = (changeSprite) ? vectors[0] : vectors[1];
		transform.localScale = new Vector3(currentVector.x, currentVector.y, 1);
	}
}
